package com.zmsp.ureportboot.config;

import com.bstek.ureport.console.UReportServlet;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 *  Ureport2 配置类
 * @author qiaolin
 * @version 2018年5月9日
 */

@ImportResource("classpath:ureport.xml")
@Configuration
@EnableAutoConfiguration
public class UreportConfig {
	
	@Bean
	public ServletRegistrationBean buildUreportServlet(){
		return new ServletRegistrationBean(new UReportServlet(), "/ureport/*");
	}

}
