package com.zmsp.ureportboot.autodatasource;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.bstek.ureport.definition.datasource.BuildinDatasource;
@Component
public class AutoDataSource3 implements BuildinDatasource {
	private  String dsName ="MyDataSource3";
	@Autowired
	@Qualifier("thirdDataSource")
	private DataSource dataSource;
	
	@Override
	public Connection getConnection() {
		
		try {
			Connection connection = dataSource.getConnection();
			return connection;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return dsName;
	}

}
