use ureport;
CREATE TABLE `ureport_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `content` mediumblob,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_ureport_file_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;